name := """social-network"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.10"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

// [info] Running Play on Java 17 is experimental. Tweaks are necessary:
// [info] https://github.com/playframework/playframework/releases/2.8.15
// these are the tweaks:
libraryDependencies ++= Seq(
  "com.google.inject" % "guice" % "5.1.0",
  "com.google.inject.extensions" % "guice-assistedinject" % "5.1.0"
)
libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "5.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "5.0.0",
  // Test
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
  jdbc,
  evolutions,
  "mysql" % "mysql-connector-java" % "8.0.32"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

// password hashing
libraryDependencies ++= Seq("org.mindrot" % "jbcrypt" % "0.3m")

// jwt
libraryDependencies += "com.pauldijou" %% "jwt-play-json" % "5.0.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.16"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.16" % "test"
libraryDependencies += "org.scalamock" %% "scalamock" % "5.2.0" % Test
