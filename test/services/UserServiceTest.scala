package services
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalamock.scalatest.AsyncMockFactory
import repositories.UserRepository
import model._
import model.dto._
import model.exceptions.user._
import scala.concurrent.Future

class UserServiceTest extends AsyncFlatSpec with AsyncMockFactory {

  val userRepo: UserRepository = mock[UserRepository]

  val blockService: BlockService = mock[BlockService]
  val passwordService: PasswordService = mock[PasswordService]
  val userService = new UserService(userRepo, blockService, passwordService)


  behavior of "password change"

  it should "say that new passwords don't match in future failure" in {
    val dto = new NewPasswordDTO("rawPassword", "password1", "password2")
    val result = userService.attemptPasswordChange(dto, 1)
    result.map(_ => fail()).recover {
      case ex: PasswordChangeFailed => assert(ex.getMessage == "New passwords don't match")
    }
  }

  it should "say old and new passwords are the same in future failure" in {
    val dto = new NewPasswordDTO("rawPassword", "rawPassword", "rawPassword")
    val result = userService.attemptPasswordChange(dto, 1)
    result.map(_ => fail()).recover {
      case ex: PasswordChangeFailed => assert(ex.getMessage == "Old and new password are the same")
    }
  }

  it should "say old passwords don't match" in {
    val preUpdateUser = new User(1, "username1", "rawPassword123")
    userRepo.getById expects 1 returning Future.successful(Some(preUpdateUser))

    val dto = new NewPasswordDTO("rawPassword", "password1", "password1")
    passwordService.checkpw expects(dto.oldPassword, preUpdateUser.password) returning false

    val result = userService.attemptPasswordChange(dto, 1)
    result.map(_ => fail()).recover {
      case ex: PasswordChangeFailed => assert(ex.getMessage == "Old passwords don't match")
    }
  }

  it should "fail to find the user" in {
    userRepo.getById expects 2 returning Future.successful(None)

    val dto = new NewPasswordDTO("rawPassword", "password1", "password1")
    val result = userService.attemptPasswordChange(dto, 2)
    result.map(_ => fail()).recover {
      case ex:PasswordChangeFailed => assert(ex.getMessage == "User doesn't exist")
    }
  }

  it should "return updated user in successful future" in {
    val preUpdateUser = new User(1, "username1", "password1")
    userRepo.getById expects 1 returning Future.successful(Some(preUpdateUser))

    val postUpdateUser = new User(1, "username1", "rawPassword")
    userRepo.update expects(1, postUpdateUser) returning Future.successful(Some(postUpdateUser))

    val dto = new NewPasswordDTO("password1", "rawPassword", "rawPassword")
    passwordService.checkpw expects(dto.oldPassword, preUpdateUser.password) returning true
    passwordService.hashpw expects dto.newPassword1 returning "rawPassword"
    val result = userService.attemptPasswordChange(dto, 1)
    result.map {
      case Some(returnedUser) => assert(returnedUser == postUpdateUser)
      case None => fail()
    }
  }

}