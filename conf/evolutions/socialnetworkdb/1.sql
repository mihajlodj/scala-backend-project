-- !Ups
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
);

CREATE TABLE `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `posterId` int NOT NULL,
  `caption` varchar(255) NOT NULL,
  `edited` tinyint NOT NULL,
  `deleted` tinyint NOT NULL,
  `datePosted` varchar(45) NOT NULL,
  `likes` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);

CREATE TABLE `comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `postId` int NOT NULL,
  `commenterId` int NOT NULL,
  `comment` varchar(255) NOT NULL,
  `datePosted` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);

CREATE TABLE `relationships` (
  `id` int NOT NULL AUTO_INCREMENT,
  `senderId` int NOT NULL,
  `receiverId` int NOT NULL,
  `status` varchar(45) NOT NULL,
  `date` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `user_post_likes` (
  `usrid` int NOT NULL,
  `postid` int NOT NULL,
  PRIMARY KEY (`usrid`,`postid`)
);

-- USER inserts
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (1,  'email1@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC');
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (2,  'email2@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC');
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (3,  'email3@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC');
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (4,  'email4@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (5,  'email5@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (6,  'email6@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (7,  'email7@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (8,  'email8@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (9,  'email9@example.com',  '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (10, 'email10@example.com', '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (11, 'email11@example.com', '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (12, 'email12@example.com', '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (13, 'email13@example.com', '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (14, 'email14@example.com', '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
INSERT INTO socialnetworkdb.users (id,username,password) VALUES (15, 'email15@example.com', '$2a$12$aZI4PU9zI0rMu1f4FNbLOe4Dg7DLKeMjKe3Mvv27zLY1OliMgCYkC' );
-- FRIENDS inserts
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (12, 2, 'PENDING', '2023-01-19 10:57:25');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (4, 7, 'PENDING', '2023-03-16 10:19:23');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (3, 5, 'ACCEPTED', '2023-02-18 03:01:08');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (2, 15, 'ACCEPTED', '2023-01-25 05:50:12');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (13, 15, 'REJECTED', '2023-05-08 01:11:37');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (11, 12, 'ACCEPTED', '2023-05-21 12:49:30');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (14, 1, 'ACCEPTED', '2023-03-10 18:48:17');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (7, 11, 'PENDING', '2023-05-07 03:21:33');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (6, 14, 'REJECTED', '2023-03-18 23:55:07');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (6, 8, 'PENDING', '2023-02-20 07:01:06');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (12, 3, 'REJECTED', '2023-03-23 07:43:42');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (13, 14, 'PENDING', '2023-01-27 20:25:23');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (10, 11, 'PENDING', '2023-04-25 23:02:54');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (9, 5, 'ACCEPTED', '2023-03-15 20:45:42');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (8, 13, 'PENDING', '2023-02-16 23:17:33');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (10, 2, 'REJECTED', '2023-03-27 13:53:45');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (3, 11, 'ACCEPTED', '2023-03-04 18:42:08');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (5, 10, 'ACCEPTED', '2023-05-06 07:50:28');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (4, 5, 'PENDING', '2023-04-28 11:36:15');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (11, 6, 'PENDING', '2023-03-30 14:54:30');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (13, 6, 'REJECTED', '2023-04-26 16:05:10');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (2, 4, 'REJECTED', '2023-04-14 04:42:17');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (6, 3, 'ACCEPTED', '2023-02-02 21:59:00');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (2, 3, 'PENDING', '2023-02-01 02:06:42');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (7, 6, 'PENDING', '2023-05-23 21:56:47');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (4, 8, 'REJECTED', '2023-01-30 21:06:20');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (1, 2, 'PENDING', '2023-02-18 19:07:08');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (4, 1, 'REJECTED', '2023-04-27 19:11:01');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (1, 9, 'PENDING', '2023-01-16 00:13:12');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (2, 11, 'ACCEPTED', '2023-02-25 11:28:23');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (14, 12, 'ACCEPTED', '2023-05-21 01:48:55');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (5, 7, 'REJECTED', '2023-03-28 22:22:18');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (14, 2, 'PENDING', '2023-05-02 11:45:57');
INSERT INTO socialnetworkdb.friend_requests (senderId,receiverId,status,dateIssued,dateAnswered) VALUES (3, 7, 'REJECTED', '2023-03-06 08:21:49');
-- POST INSERTS
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (3, 1, 'ovo je tekst posta 3', 0, 0, '2023-01-12 15:35:10', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (4, 14, 'ovo je tekst posta 4', 0, 0, '2023-03-01 15:15:17', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (5, 5, 'ovo je tekst posta 5', 0, 0, '2023-05-16 18:19:58', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (6, 3, 'ovo je tekst posta 6', 0, 0, '2023-04-25 11:31:11', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (7, 4, 'ovo je tekst posta 7', 0, 0, '2023-04-19 22:34:28', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (8, 15, 'ovo je tekst posta 8', 0, 0, '2023-03-15 10:17:17', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (9, 13, 'ovo je tekst posta 9', 0, 0, '2023-05-24 18:53:50', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (10, 15, 'ovo je tekst posta 10', 0, 0, '2023-03-11 19:47:06', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (11, 12, 'ovo je tekst posta 11', 0, 0, '2023-05-24 16:25:48', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (12, 11, 'ovo je tekst posta 12', 0, 0, '2023-03-22 19:58:07', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (13, 10, 'ovo je tekst posta 13', 0, 0, '2023-05-21 18:55:09', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (14, 11, 'ovo je tekst posta 14', 0, 0, '2023-02-10 10:13:05', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (15, 9, 'ovo je tekst posta 15', 0, 0, '2023-05-28 10:47:14', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (16, 3, 'ovo je tekst posta 16', 0, 0, '2023-05-18 12:07:23', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (17, 1, 'ovo je tekst posta 17', 0, 0, '2023-02-06 08:18:00', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (18, 12, 'ovo je tekst posta 18', 0, 0, '2023-02-19 00:34:43', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (19, 5, 'ovo je tekst posta 19', 0, 0, '2023-03-24 12:40:25', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (20, 11, 'ovo je tekst posta 20', 0, 0, '2023-01-09 15:09:20', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (21, 8, 'ovo je tekst posta 21', 0, 0, '2023-04-13 10:06:24', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (22, 7, 'ovo je tekst posta 22', 0, 0, '2023-03-29 16:53:47', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (23, 2, 'ovo je tekst posta 23', 0, 0, '2023-05-05 17:58:57', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (24, 7, 'ovo je tekst posta 24', 0, 0, '2023-04-14 18:13:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (25, 6, 'ovo je tekst posta 25', 0, 0, '2023-01-28 01:38:07', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (26, 7, 'ovo je tekst posta 26', 0, 0, '2023-01-03 18:42:15', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (27, 1, 'ovo je tekst posta 27', 0, 0, '2023-02-02 22:40:45', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (28, 8, 'ovo je tekst posta 28', 0, 0, '2023-02-23 15:20:44', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (29, 10, 'ovo je tekst posta 29', 0, 0, '2023-01-27 15:55:41', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (30, 14, 'ovo je tekst posta 30', 0, 0, '2023-03-30 01:44:48', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (31, 7, 'ovo je tekst posta 31', 0, 0, '2023-01-21 15:05:58', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (32, 4, 'ovo je tekst posta 32', 0, 0, '2023-01-27 00:47:37', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (33, 5, 'ovo je tekst posta 33', 0, 0, '2023-03-05 15:14:33', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (34, 9, 'ovo je tekst posta 34', 0, 0, '2023-04-13 21:48:46', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (35, 15, 'ovo je tekst posta 35', 0, 0, '2023-01-09 21:27:03', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (36, 2, 'ovo je tekst posta 36', 0, 0, '2023-02-01 09:58:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (37, 13, 'ovo je tekst posta 37', 0, 0, '2023-02-15 07:35:22', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (38, 2, 'ovo je tekst posta 38', 0, 0, '2023-04-07 07:18:57', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (39, 8, 'ovo je tekst posta 39', 0, 0, '2023-05-22 04:12:11', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (40, 11, 'ovo je tekst posta 40', 0, 0, '2023-03-18 06:22:26', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (41, 8, 'ovo je tekst posta 41', 0, 0, '2023-03-29 05:29:12', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (42, 2, 'ovo je tekst posta 42', 0, 0, '2023-04-07 06:21:32', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (43, 6, 'ovo je tekst posta 43', 0, 0, '2023-05-04 18:21:29', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (44, 2, 'ovo je tekst posta 44', 0, 0, '2023-01-04 19:38:42', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (45, 4, 'ovo je tekst posta 45', 0, 0, '2023-01-17 20:50:48', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (46, 10, 'ovo je tekst posta 46', 0, 0, '2023-01-19 16:33:50', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (47, 14, 'ovo je tekst posta 47', 0, 0, '2023-04-29 05:52:53', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (48, 14, 'ovo je tekst posta 48', 0, 0, '2023-02-24 20:43:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (49, 4, 'ovo je tekst posta 49', 0, 0, '2023-02-05 01:29:14', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (50, 12, 'ovo je tekst posta 50', 0, 0, '2023-03-22 19:24:14', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (51, 11, 'ovo je tekst posta 51', 0, 0, '2023-03-31 16:16:14', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (52, 2, 'ovo je tekst posta 52', 0, 0, '2023-03-02 16:13:45', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (53, 9, 'ovo je tekst posta 53', 0, 0, '2023-04-23 21:08:52', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (54, 2, 'ovo je tekst posta 54', 0, 0, '2023-05-07 11:45:34', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (55, 14, 'ovo je tekst posta 55', 0, 0, '2023-05-08 02:50:52', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (56, 12, 'ovo je tekst posta 56', 0, 0, '2023-05-27 07:18:00', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (57, 15, 'ovo je tekst posta 57', 0, 0, '2023-03-26 10:55:22', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (58, 7, 'ovo je tekst posta 58', 0, 0, '2023-03-06 00:41:52', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (59, 14, 'ovo je tekst posta 59', 0, 0, '2023-03-07 03:56:41', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (60, 9, 'ovo je tekst posta 60', 0, 0, '2023-04-13 13:01:20', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (61, 11, 'ovo je tekst posta 61', 0, 0, '2023-01-28 01:08:31', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (62, 3, 'ovo je tekst posta 62', 0, 0, '2023-04-26 09:55:16', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (63, 8, 'ovo je tekst posta 63', 0, 0, '2023-02-19 15:53:30', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (64, 3, 'ovo je tekst posta 64', 0, 0, '2023-05-17 11:18:32', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (65, 2, 'ovo je tekst posta 65', 0, 0, '2023-04-19 09:58:52', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (66, 8, 'ovo je tekst posta 66', 0, 0, '2023-04-03 22:37:09', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (67, 8, 'ovo je tekst posta 67', 0, 0, '2023-02-03 19:49:24', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (68, 11, 'ovo je tekst posta 68', 0, 0, '2023-04-11 15:41:48', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (69, 5, 'ovo je tekst posta 69', 0, 0, '2023-03-24 09:30:03', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (70, 8, 'ovo je tekst posta 70', 0, 0, '2023-01-20 21:50:14', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (71, 15, 'ovo je tekst posta 71', 0, 0, '2023-04-20 16:15:48', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (72, 4, 'ovo je tekst posta 72', 0, 0, '2023-05-14 00:25:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (73, 13, 'ovo je tekst posta 73', 0, 0, '2023-01-04 11:17:05', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (74, 10, 'ovo je tekst posta 74', 0, 0, '2023-03-18 03:30:30', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (75, 12, 'ovo je tekst posta 75', 0, 0, '2023-02-06 10:52:57', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (76, 13, 'ovo je tekst posta 76', 0, 0, '2023-01-14 13:10:05', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (77, 4, 'ovo je tekst posta 77', 0, 0, '2023-03-18 00:09:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (78, 1, 'ovo je tekst posta 78', 0, 0, '2023-01-13 05:31:27', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (79, 8, 'ovo je tekst posta 79', 0, 0, '2023-05-28 12:51:21', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (80, 3, 'ovo je tekst posta 80', 0, 0, '2023-01-20 06:08:23', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (81, 13, 'ovo je tekst posta 81', 0, 0, '2023-04-10 13:17:39', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (82, 1, 'ovo je tekst posta 82', 0, 0, '2023-05-26 16:16:40', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (83, 15, 'ovo je tekst posta 83', 0, 0, '2023-05-22 17:27:42', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (84, 2, 'ovo je tekst posta 84', 0, 0, '2023-04-17 11:02:22', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (85, 8, 'ovo je tekst posta 85', 0, 0, '2023-05-23 05:12:08', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (86, 7, 'ovo je tekst posta 86', 0, 0, '2023-04-05 18:24:54', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (87, 15, 'ovo je tekst posta 87', 0, 0, '2023-04-20 01:12:32', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (88, 12, 'ovo je tekst posta 88', 0, 0, '2023-01-20 14:37:24', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (89, 4, 'ovo je tekst posta 89', 0, 0, '2023-03-19 05:47:49', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (90, 10, 'ovo je tekst posta 90', 0, 0, '2023-04-05 19:35:12', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (91, 14, 'ovo je tekst posta 91', 0, 0, '2023-01-01 10:13:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (92, 15, 'ovo je tekst posta 92', 0, 0, '2023-03-23 15:03:40', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (93, 14, 'ovo je tekst posta 93', 0, 0, '2023-04-29 03:17:30', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (94, 11, 'ovo je tekst posta 94', 0, 0, '2023-03-19 00:55:31', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (95, 5, 'ovo je tekst posta 95', 0, 0, '2023-04-19 17:41:54', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (96, 3, 'ovo je tekst posta 96', 0, 0, '2023-05-25 21:32:06', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (97, 12, 'ovo je tekst posta 97', 0, 0, '2023-03-04 07:31:28', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (98, 7, 'ovo je tekst posta 98', 0, 0, '2023-04-04 23:53:00', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (99, 8, 'ovo je tekst posta 99', 0, 0, '2023-04-25 06:30:25', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (100, 13, 'ovo je tekst posta 100', 0, 0, '2023-02-08 03:45:20', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (101, 11, 'ovo je tekst posta 101', 0, 0, '2023-05-25 16:04:27', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (102, 6, 'ovo je tekst posta 102', 0, 0, '2023-04-15 01:01:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (103, 3, 'ovo je tekst posta 103', 0, 0, '2023-04-07 14:36:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (104, 2, 'ovo je tekst posta 104', 0, 0, '2023-04-27 14:18:52', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (105, 4, 'ovo je tekst posta 105', 0, 0, '2023-01-08 02:33:10', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (106, 8, 'ovo je tekst posta 106', 0, 0, '2023-01-23 21:52:26', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (107, 1, 'ovo je tekst posta 107', 0, 0, '2023-01-04 00:12:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (108, 7, 'ovo je tekst posta 108', 0, 0, '2023-02-11 11:16:25', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (109, 6, 'ovo je tekst posta 109', 0, 0, '2023-04-13 06:11:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (110, 13, 'ovo je tekst posta 110', 0, 0, '2023-01-31 19:55:41', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (111, 2, 'ovo je tekst posta 111', 0, 0, '2023-01-19 03:44:44', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (112, 9, 'ovo je tekst posta 112', 0, 0, '2023-03-06 04:29:32', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (113, 9, 'ovo je tekst posta 113', 0, 0, '2023-01-03 08:35:55', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (114, 15, 'ovo je tekst posta 114', 0, 0, '2023-04-24 23:26:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (115, 4, 'ovo je tekst posta 115', 0, 0, '2023-02-20 16:41:53', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (116, 2, 'ovo je tekst posta 116', 0, 0, '2023-02-19 00:28:03', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (117, 8, 'ovo je tekst posta 117', 0, 0, '2023-04-28 07:53:18', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (118, 2, 'ovo je tekst posta 118', 0, 0, '2023-04-21 18:02:11', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (119, 2, 'ovo je tekst posta 119', 0, 0, '2023-03-07 16:07:06', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (120, 4, 'ovo je tekst posta 120', 0, 0, '2023-02-23 01:28:49', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (121, 11, 'ovo je tekst posta 121', 0, 0, '2023-04-13 08:40:35', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (122, 5, 'ovo je tekst posta 122', 0, 0, '2023-05-19 17:09:01', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (123, 12, 'ovo je tekst posta 123', 0, 0, '2023-03-18 17:25:26', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (124, 9, 'ovo je tekst posta 124', 0, 0, '2023-01-20 20:46:51', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (125, 10, 'ovo je tekst posta 125', 0, 0, '2023-01-19 03:01:46', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (126, 11, 'ovo je tekst posta 126', 0, 0, '2023-04-29 00:24:27', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (127, 12, 'ovo je tekst posta 127', 0, 0, '2023-02-15 03:50:13', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (128, 11, 'ovo je tekst posta 128', 0, 0, '2023-03-09 06:08:37', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (129, 10, 'ovo je tekst posta 129', 0, 0, '2023-05-04 06:43:01', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (130, 8, 'ovo je tekst posta 130', 0, 0, '2023-01-19 09:30:03', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (131, 3, 'ovo je tekst posta 131', 0, 0, '2023-01-18 13:00:18', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (132, 1, 'ovo je tekst posta 132', 0, 0, '2023-01-01 23:59:06', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (133, 14, 'ovo je tekst posta 133', 0, 0, '2023-01-10 01:29:06', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (134, 2, 'ovo je tekst posta 134', 0, 0, '2023-03-25 03:07:08', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (135, 8, 'ovo je tekst posta 135', 0, 0, '2023-02-20 00:39:06', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (136, 13, 'ovo je tekst posta 136', 0, 0, '2023-03-24 10:07:16', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (137, 12, 'ovo je tekst posta 137', 0, 0, '2023-05-03 04:05:20', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (138, 9, 'ovo je tekst posta 138', 0, 0, '2023-02-20 06:50:17', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (139, 9, 'ovo je tekst posta 139', 0, 0, '2023-01-15 01:27:39', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (140, 1, 'ovo je tekst posta 140', 0, 0, '2023-01-12 08:42:56', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (141, 3, 'ovo je tekst posta 141', 0, 0, '2023-05-02 23:23:45', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (142, 11, 'ovo je tekst posta 142', 0, 0, '2023-02-10 00:48:12', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (143, 8, 'ovo je tekst posta 143', 0, 0, '2023-03-06 08:30:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (144, 5, 'ovo je tekst posta 144', 0, 0, '2023-01-30 20:24:49', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (145, 6, 'ovo je tekst posta 145', 0, 0, '2023-01-22 13:27:56', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (146, 11, 'ovo je tekst posta 146', 0, 0, '2023-04-19 06:11:12', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (147, 7, 'ovo je tekst posta 147', 0, 0, '2023-01-26 16:20:58', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (148, 1, 'ovo je tekst posta 148', 0, 0, '2023-03-19 11:51:07', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (149, 8, 'ovo je tekst posta 149', 0, 0, '2023-04-21 16:37:39', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (150, 10, 'ovo je tekst posta 150', 0, 0, '2023-04-01 05:29:46', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (151, 11, 'ovo je tekst posta 151', 0, 0, '2023-05-23 11:49:13', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (152, 14, 'ovo je tekst posta 152', 0, 0, '2023-04-05 07:12:16', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (153, 12, 'ovo je tekst posta 153', 0, 0, '2023-04-23 22:17:00', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (154, 3, 'ovo je tekst posta 154', 0, 0, '2023-04-01 10:50:48', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (155, 2, 'ovo je tekst posta 155', 0, 0, '2023-01-06 20:32:20', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (156, 13, 'ovo je tekst posta 156', 0, 0, '2023-05-10 21:28:40', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (157, 2, 'ovo je tekst posta 157', 0, 0, '2023-03-29 09:03:29', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (158, 11, 'ovo je tekst posta 158', 0, 0, '2023-03-02 09:15:32', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (159, 11, 'ovo je tekst posta 159', 0, 0, '2023-02-18 02:14:29', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (160, 12, 'ovo je tekst posta 160', 0, 0, '2023-03-10 01:43:53', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (161, 4, 'ovo je tekst posta 161', 0, 0, '2023-04-05 23:07:04', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (162, 6, 'ovo je tekst posta 162', 0, 0, '2023-02-10 20:33:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (163, 2, 'ovo je tekst posta 163', 0, 0, '2023-02-13 10:29:27', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (164, 14, 'ovo je tekst posta 164', 0, 0, '2023-04-28 12:52:23', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (165, 5, 'ovo je tekst posta 165', 0, 0, '2023-05-01 13:45:49', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (166, 6, 'ovo je tekst posta 166', 0, 0, '2023-05-22 16:36:19', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (167, 9, 'ovo je tekst posta 167', 0, 0, '2023-02-15 08:57:05', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (168, 8, 'ovo je tekst posta 168', 0, 0, '2023-04-02 06:21:37', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (169, 14, 'ovo je tekst posta 169', 0, 0, '2023-02-20 17:24:07', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (170, 10, 'ovo je tekst posta 170', 0, 0, '2023-02-10 15:32:23', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (171, 8, 'ovo je tekst posta 171', 0, 0, '2023-03-08 06:56:28', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (172, 12, 'ovo je tekst posta 172', 0, 0, '2023-03-19 22:18:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (173, 13, 'ovo je tekst posta 173', 0, 0, '2023-02-26 02:20:29', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (174, 15, 'ovo je tekst posta 174', 0, 0, '2023-05-24 23:13:14', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (175, 3, 'ovo je tekst posta 175', 0, 0, '2023-01-10 13:19:10', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (176, 5, 'ovo je tekst posta 176', 0, 0, '2023-03-03 04:25:01', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (177, 14, 'ovo je tekst posta 177', 0, 0, '2023-03-06 08:05:09', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (178, 8, 'ovo je tekst posta 178', 0, 0, '2023-02-20 10:09:52', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (179, 11, 'ovo je tekst posta 179', 0, 0, '2023-01-17 23:22:58', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (180, 11, 'ovo je tekst posta 180', 0, 0, '2023-02-22 00:19:08', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (181, 1, 'ovo je tekst posta 181', 0, 0, '2023-05-05 06:57:44', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (182, 1, 'ovo je tekst posta 182', 0, 0, '2023-03-16 23:50:09', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (183, 1, 'ovo je tekst posta 183', 0, 0, '2023-04-18 01:30:38', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (184, 14, 'ovo je tekst posta 184', 0, 0, '2023-02-04 14:18:54', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (185, 8, 'ovo je tekst posta 185', 0, 0, '2023-02-11 01:09:17', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (186, 9, 'ovo je tekst posta 186', 0, 0, '2023-05-15 07:35:55', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (187, 8, 'ovo je tekst posta 187', 0, 0, '2023-05-24 07:39:26', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (188, 11, 'ovo je tekst posta 188', 0, 0, '2023-01-24 15:37:31', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (189, 1, 'ovo je tekst posta 189', 0, 0, '2023-01-05 08:45:26', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (190, 3, 'ovo je tekst posta 190', 0, 0, '2023-03-28 01:40:28', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (191, 3, 'ovo je tekst posta 191', 0, 0, '2023-05-24 11:48:13', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (192, 6, 'ovo je tekst posta 192', 0, 0, '2023-02-04 03:30:56', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (193, 10, 'ovo je tekst posta 193', 0, 0, '2023-05-16 09:46:10', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (194, 6, 'ovo je tekst posta 194', 0, 0, '2023-02-21 13:45:02', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (195, 10, 'ovo je tekst posta 195', 0, 0, '2023-03-30 00:18:30', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (196, 2, 'ovo je tekst posta 196', 0, 0, '2023-03-08 10:59:01', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (197, 9, 'ovo je tekst posta 197', 0, 0, '2023-03-27 09:35:43', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (198, 12, 'ovo je tekst posta 198', 0, 0, '2023-05-12 08:34:27', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (199, 3, 'ovo je tekst posta 199', 0, 0, '2023-03-13 22:50:04', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (200, 7, 'ovo je tekst posta 200', 0, 0, '2023-04-09 22:31:07', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (201, 8, 'ovo je tekst posta 201', 0, 0, '2023-02-02 13:09:41', 0);
INSERT INTO socialnetworkdb.posts (id,posterId,caption,edited,deleted,datePosted,likes) VALUES (202, 14, 'ovo je tekst posta 202', 0, 0, '2023-05-11 11:30:58', 0);

-- !Downs

DROP TABLE users;
DROP TABLE posts;
DROP TABLE comments;
DROP TABLE relationships;
DROP TABLE user_post_likes;