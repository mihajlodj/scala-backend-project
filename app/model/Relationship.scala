package model

import model.RelationStatus.RelationStatus
import slick.jdbc.{GetResult, PositionedResult}

import java.time.format.DateTimeFormatter
import java.time.LocalDateTime

object RelationStatus extends Enumeration {
  type RelationStatus = Value
  val ACCEPTED: model.RelationStatus.Value = Value("ACCEPTED")
  val REJECTED: model.RelationStatus.Value = Value("REJECTED")
  val PENDING: model.RelationStatus.Value = Value("PENDING")
  val BLOCKED: model.RelationStatus.Value = Value("BLOCKED")

  def apply(status: String): RelationStatus = {
    status match {
      case "ACCEPTED" => RelationStatus.ACCEPTED
      case "REJECTED" => RelationStatus.REJECTED
      case "PENDING" => RelationStatus.PENDING
      case "BLOCKED" => RelationStatus.BLOCKED
      case _ => throw new RuntimeException("Request status string must be ACCEPTED | REJECTED | PENDING | BLOCKED")
    }
  }

  def friendshipStatuses(): Seq[RelationStatus] = {
    Seq(RelationStatus.ACCEPTED, RelationStatus.REJECTED, RelationStatus.PENDING)
  }

  def unapply(status: RelationStatus): Option[String] = {
    status match {
      case RelationStatus.ACCEPTED => Some("ACCEPTED")
      case RelationStatus.REJECTED => Some("REJECTED")
      case RelationStatus.PENDING => Some("PENDING")
      case RelationStatus.BLOCKED => Some("BLOCKED")
      case _ => None
    }
  }

  implicit val getRequestStatusResult: AnyRef with GetResult[RelationStatus] = GetResult(r =>
    RelationStatus(r.nextInt()))
}

case class Relationship(id: Int, senderId: Int, receiverId: Int, status: RelationStatus, date: LocalDateTime)

object Relationship {

  implicit val getFriendRequestResult: AnyRef with GetResult[Relationship] = GetResult(r =>
    Relationship(r.<<, r.<<, r.<<, r.<<, toLocalDateTime(r)))

  private def toLocalDateTime(r: PositionedResult) = {
    val format = "yyyy-MM-dd'T'HH:mm:ss.nn"
    val formatter = DateTimeFormatter.ofPattern(format)
    LocalDateTime.parse(r.nextString(), formatter)
  }
  def tupled: ((Int, Int, Int, RelationStatus, LocalDateTime)) => Relationship = (Relationship.apply _).tupled
}
