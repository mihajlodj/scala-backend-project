package model

import play.api.libs.json.{Json, OFormat}
import slick.jdbc.GetResult

case class UserPostLikes(usrid: Int, postid: Int)

object UserPostLikes {
  implicit val jsonFormat: OFormat[UserPostLikes] = Json.format[UserPostLikes]
  implicit val getPostResult: AnyRef with GetResult[UserPostLikes] = GetResult(r => UserPostLikes(r.<<, r.<<))

  def tupled: ((Int, Int)) => UserPostLikes = (UserPostLikes.apply _).tupled
}