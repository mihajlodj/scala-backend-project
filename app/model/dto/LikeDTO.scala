package model.dto

import play.api.libs.json.{Json, OWrites, Reads}

case class LikeDTO(postid: Int, posterid: Int)

object LikeDTO {
  implicit val likeReads: Reads[LikeDTO] = Json.reads[LikeDTO]
  implicit val likeWrites: OWrites[LikeDTO] = Json.writes[LikeDTO]
}

