package model.dto

import play.api.libs.json.{Json, OFormat}

case class CommentDTO(comment: String, postId: Int, id: Option[Int])

object CommentDTO {
  implicit val jsonFormat: OFormat[CommentDTO] = Json.format[CommentDTO]

}