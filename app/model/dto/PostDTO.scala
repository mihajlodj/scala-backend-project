package model.dto

import play.api.libs.json.{Json, Reads}

case class PostDTO(id: Int = -1, caption: String) {

}
object PostDTO {
  implicit val loginReads: Reads[PostDTO] = Json.reads[PostDTO]
}
