package model.dto

import play.api.libs.json.{Json, Reads}

case class RegistrationDTO(
  username: String,
  password1: String,
  password2: String
)

object RegistrationDTO {
  implicit val registrationReads: Reads[RegistrationDTO] = Json.reads[RegistrationDTO]
}

