package model.dto

import play.api.libs.json.{Json, Reads}

case class LoginDTO(
  username: String,
  password: String
)

object LoginDTO {
  implicit val loginReads: Reads[LoginDTO] = Json.reads[LoginDTO]
}
