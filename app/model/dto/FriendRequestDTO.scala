package model.dto

import model.Relationship
import play.api.libs.json.{Json, OWrites, Reads}

import java.time.LocalDateTime

case class FriendRequestDTO(receiverId: Int, id: Option[Int], senderId: Option[Int], date: Option[LocalDateTime])

object FriendRequestDTO {
  implicit val requestWrites: OWrites[FriendRequestDTO] = Json.writes[FriendRequestDTO]
  implicit val requestReads: Reads[FriendRequestDTO] = Json.reads[FriendRequestDTO]

  def fromRequest(r: Relationship): FriendRequestDTO = {
    new FriendRequestDTO(r.receiverId, Option(r.id), Option(r.senderId), Option(r.date))
  }
}
