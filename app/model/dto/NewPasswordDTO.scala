package model.dto

import play.api.libs.json.{Json, Reads}

case class NewPasswordDTO(oldPassword: String, newPassword1: String, newPassword2: String)

object NewPasswordDTO {
  implicit val loginReads: Reads[NewPasswordDTO] = Json.reads[NewPasswordDTO]
}