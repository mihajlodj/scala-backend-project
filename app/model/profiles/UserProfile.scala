package model.profiles
import model.User
import play.api.libs.json.{Json, OWrites, Reads}
case class UserProfile(
  id: Int,
  username: String
)


object UserProfile {

  implicit val userProfileWrites: OWrites[UserProfile] = Json.writes[UserProfile]
  implicit val userProfileReads: Reads[UserProfile] = Json.reads[UserProfile]

  def fromUser(u: User ): UserProfile = {
    UserProfile(u.id, u.username)
  }
}