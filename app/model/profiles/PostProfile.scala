package model.profiles

import play.api.libs.json.{Json, OWrites, Reads}
import slick.jdbc.{GetResult, PositionedResult}

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

case class PostProfile(id: Int = -1, posterId: Int, caption: String, edited: Boolean,
                           deleted: Boolean, datePosted: LocalDateTime, likes: Int = 0, userLiked: Boolean, username: String)
object PostProfile {
  implicit val timelineReads: Reads[PostProfile] = Json.reads[PostProfile]
  implicit val timelineWrites: OWrites[PostProfile] = Json.writes[PostProfile]
  implicit val getPostResult: AnyRef with GetResult[PostProfile] =
    GetResult(r => PostProfile(r.<<, r.<<, r.<<, r.<<, r.<<, toLocalDateTime(r), r.<<, r.<<, r.<<))

  private def toLocalDateTime(r: PositionedResult) = {
    val format = "yyyy-MM-dd'T'HH:mm:ss.nn"
    val formatter = DateTimeFormatter.ofPattern(format)
    LocalDateTime.parse(r.nextString(), formatter)
  }
}
