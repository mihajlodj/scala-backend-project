package model

import play.api.libs.json.{Json, OFormat}
import slick.jdbc.{GetResult, PositionedResult}

import java.time.format.DateTimeFormatter
import java.time.LocalDateTime

case class Post(id: Int = -1, posterId: Int, caption: String, edited: Boolean, deleted: Boolean, datePosted: LocalDateTime, likes: Int = 0)

object Post {
  implicit val jsonFormat: OFormat[Post] = Json.format[Post]
  implicit val getPostResult: AnyRef with GetResult[Post] = GetResult(r => Post(r.<<, r.<<, r.<<, r.<<, r.<<, toLocalDateTime(r)))

  private def toLocalDateTime(r: PositionedResult) = {
    val format = "yyyy-MM-dd'T'HH:mm:ss.nn"
    val formatter = DateTimeFormatter.ofPattern(format)
    LocalDateTime.parse(r.nextString(), formatter)
  }
  def tupled: ((Int, Int, String, Boolean, Boolean, LocalDateTime, Int)) => Post = (Post.apply _).tupled
}