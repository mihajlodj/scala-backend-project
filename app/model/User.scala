package model

import play.api.libs.json.{Json, OFormat}
import slick.jdbc.GetResult

case class User(id: Int, username: String, password: String)

object User {
  implicit val jsonFormat: OFormat[User] = Json.format[User]
  implicit val getUserResult: AnyRef with GetResult[User] = GetResult(r => User(r.<<, r.<<, r.<<))
  def tupled: ((Int, String, String)) => User = (User.apply _).tupled

}
