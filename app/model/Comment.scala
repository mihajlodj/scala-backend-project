package model

import play.api.libs.json.{Json, OFormat}
import slick.jdbc.{GetResult, PositionedResult}

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

case class Comment(id: Int, postId: Int, commenterId: Int, comment: String, datePosted: LocalDateTime)

object Comment {
  implicit val jsonFormat: OFormat[Comment] = Json.format[Comment]

  implicit val getCommentResult: AnyRef with GetResult[Comment] = GetResult(r =>
    Comment(r.<<, r.<<, r.<<, r.<<, toLocalDateTime(r)))

  private def toLocalDateTime(r: PositionedResult) = {
    val format = "yyyy-MM-dd'T'HH:mm:ss.nn"
    val formatter = DateTimeFormatter.ofPattern(format)
    LocalDateTime.parse(r.nextString(), formatter)
  }
  def tupled: ((Int, Int, Int, String, LocalDateTime)) => Comment = (Comment.apply _).tupled

}
