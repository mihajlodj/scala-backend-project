package model.exceptions.likes

class LikeDoesntExistException(s: String) extends Exception(s)