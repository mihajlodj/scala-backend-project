package model.exceptions.likes

class PostLikedException(s: String) extends Exception(s)