package model.exceptions.block

class BlockFailedException(s: String) extends Exception(s)
