package model.exceptions.user

class PasswordChangeFailed(s: String) extends Exception(s) {
  def equals(other: PasswordChangeFailed): Boolean = {
    s == other.getMessage
  }
}