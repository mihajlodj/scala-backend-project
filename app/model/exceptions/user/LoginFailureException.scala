package model.exceptions.user

class LoginFailureException(s: String) extends Exception(s)
