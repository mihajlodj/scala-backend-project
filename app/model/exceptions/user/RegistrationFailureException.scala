package model.exceptions.user

class RegistrationFailureException (s: String) extends Exception(s)