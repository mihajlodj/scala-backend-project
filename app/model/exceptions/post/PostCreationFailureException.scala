package model.exceptions.post

case class PostCreationFailureException(s: String) extends Exception(s)
