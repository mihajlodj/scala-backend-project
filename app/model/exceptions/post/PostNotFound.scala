package model.exceptions.post

class PostNotFound(s: String) extends Exception(s)
