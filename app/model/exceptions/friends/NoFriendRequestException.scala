package model.exceptions.friends

class NoFriendRequestException(s: String) extends Exception(s)
