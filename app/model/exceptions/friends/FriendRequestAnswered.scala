package model.exceptions.friends

class FriendRequestAnswered(s: String) extends Exception(s)
