package model.exceptions.friends

class NotFriendsException(s: String) extends Exception(s)
