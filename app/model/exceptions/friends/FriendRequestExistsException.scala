package model.exceptions.friends

class FriendRequestExistsException(s: String) extends Exception(s)
