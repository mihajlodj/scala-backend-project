package services

import org.mindrot.jbcrypt.BCrypt

import javax.inject.{Inject, Singleton}

@Singleton
class PasswordService @Inject() () {

  def hashpw(newPassword1: String): String = {
    BCrypt.hashpw(newPassword1, BCrypt.gensalt)
  }

  def checkpw(oldPassword1: String, oldPassword2: String): Boolean = {
    BCrypt.checkpw(oldPassword1, oldPassword2)
  }

}
