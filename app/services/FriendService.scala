package services

import model.RelationStatus.RelationStatus
import model.{Relationship, RelationStatus, User}
import model.exceptions.friends.{FriendRequestAnswered, FriendRequestExistsException, NoFriendRequestException, NotFriendsException}
import repositories.{RelationsRepository, UserRepository}

import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
@Singleton
class FriendService @Inject() (friendRepository: RelationsRepository, userRepository: UserRepository) {
  def getUserFriends(usrid: Int): Future[Seq[User]] = {
    friendRepository.getAcceptedFriendRequests(usrid)
  }

  private def respondToRequest(requestId: Int, receiverId: Int, response: RelationStatus) = {
    friendRepository.getById(requestId).flatMap {
      case Some(request) =>
        if (request.status == RelationStatus.ACCEPTED || request.status == RelationStatus.REJECTED) {
          throw new FriendRequestAnswered("You have already responded to this request")
        }
        val acceptedRequest = request.copy(status = response, date = LocalDateTime.now())
        friendRepository.respondToRequest(requestId, acceptedRequest, receiverId)
      case None => throw new NoFriendRequestException("There is no such friend request")
    }
  }

  def acceptRequest(requestId: Int, receiverId: Int): Future[Option[Relationship]] = {
    respondToRequest(requestId, receiverId, RelationStatus.ACCEPTED)
  }

  def rejectRequest(receiverId: Int, requestId: Int): Future[Option[Relationship]] = {
    respondToRequest(requestId, receiverId, RelationStatus.REJECTED)
  }

  def getPendingFriendRequests(receiverId: Int): Future[Seq[Relationship]] = {
    friendRepository.getByReceiverIdAndStatus(receiverId, RelationStatus.PENDING)
  }

  def getFriendProfile(userid: Int, friendid: Int): Future[Option[User]] = {
    friendRepository.getByUsersAndStatus(userid, friendid, Seq(RelationStatus.ACCEPTED)).flatMap {
      result => if (result.isEmpty) {throw new NotFriendsException("You are not friends with this person")}
        userRepository.getById(friendid)
    }
  }

  def getFriends(usrid: Int): Future[Seq[Int]] = {
    friendRepository.getUserFriends(usrid)
  }


  def sendRequest(senderId: Int, receiverId: Int): Future[Int] = {
    val newRequest = new Relationship(-1, senderId, receiverId, RelationStatus.PENDING, LocalDateTime.now())

    friendRepository.getByUsersAndStatus(senderId, receiverId, Seq(RelationStatus.PENDING, RelationStatus.ACCEPTED, RelationStatus.BLOCKED)).flatMap {
      result => if (result.nonEmpty) {throw new FriendRequestExistsException("You cannot have multiple relations with the same person")}
        friendRepository.insert(newRequest)
    }
  }

  def areFriends(user1: Int, user2: Int): Future[Boolean] = {
    friendRepository.getByUsersAndStatus(user1, user2, Seq(RelationStatus.ACCEPTED)).map {
      result => result.nonEmpty
    }
  }

}
