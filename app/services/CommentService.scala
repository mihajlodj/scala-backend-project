package services

import model.Comment
import repositories.CommentRepository

import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class CommentService @Inject() (commentRepository: CommentRepository) {
  def getComments(postId: Int): Future[Seq[Comment]] = {
    commentRepository.getPostComments(postId)
  }


  def postComment(commenterId: Int, postId: Int, comment: String): Future[Int] = {
    val newComment = new Comment(-1, postId = postId, commenterId = commenterId, comment = comment, LocalDateTime.now())
    commentRepository.insert(newComment)
  }

  def deleteComment(id: Int, commenterId: Int): Future[Option[Int]] = commentRepository.delete(id, commenterId)

  def updateComment(commentId: Int, commenterId: Int, newComment: String): Future[Option[Comment]] = {
    commentRepository.getById(commentId).flatMap {
      case Some(c) =>
        val updatedComment = c.copy(comment = newComment)
        commentRepository.update(commentId, updatedComment, commenterId)
      case None => Future.successful(None)
    }
  }

}
