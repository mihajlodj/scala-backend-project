package services
import model.UserPostLikes
import model.exceptions.friends.NotFriendsException
import model.exceptions.likes.{LikeDoesntExistException, PostLikedException}
import repositories.{LikesRepository, PostRepository}

import java.sql.SQLIntegrityConstraintViolationException
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class LikesService @Inject() (likesRepository: LikesRepository, friendService: FriendService, postRepository: PostRepository) {


  def like(usrid: Int, postid: Int, posterId: Int): Future[Any] = {
    friendService.areFriends(usrid, posterId).flatMap {
      areFriends => if (areFriends) {
        val newLike = new UserPostLikes(usrid, postid)
        likesRepository.insert(newLike).flatMap({
          _ => postRepository.incrementLikes(postid)
        }).recover {
          case e: SQLIntegrityConstraintViolationException =>
            throw new PostLikedException("You already liked this post!")
        }
      } else {
        throw new NotFriendsException("You can't like post of someone who isn't your friend!")
      }

    }
  }

  def unlike(usrid: Int, postid: Int, posterId: Int): Future[Any] = {
    friendService.areFriends(usrid, posterId).flatMap {
      areFriends => if (areFriends) {
        likesRepository.delete(usrid, postid).flatMap {
          case false => throw new LikeDoesntExistException("You haven't liked this post!")
          case true => postRepository.decrementLikes(postid)
        }
      } else {
        throw new NotFriendsException("You can't unlike post of someone who isn't your friend!")
      }

    }
  }
}
