package services

import repositories.RelationsRepository
import model.{RelationStatus, Relationship, User}
import model.exceptions.block.BlockFailedException

import scala.concurrent.ExecutionContext.Implicits.global
import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class BlockService @Inject() (relationsRepository: RelationsRepository) {

  def block(blockerId: Int, userToBlock: Int): Future[Int] = {

    relationsRepository.getBySenderAndReceiverId(blockerId, userToBlock).flatMap {
      case Some(request) =>
        if (request.status == RelationStatus.BLOCKED) {
          throw new BlockFailedException("User is already blocked")
        }
        // proceed to block
        val blocked = request.copy(status = RelationStatus.BLOCKED, date=LocalDateTime.now())
        relationsRepository.update(blocked)
        // ako je drugi user slao zahteve prvom
        relationsRepository.deleteWithStatus(userToBlock, blockerId, RelationStatus.friendshipStatuses())
      case None =>
        val newBlockListEntry = new Relationship(-1, blockerId, userToBlock, RelationStatus.BLOCKED, LocalDateTime.now())
        relationsRepository.insert(newBlockListEntry)
        relationsRepository.deleteWithStatus(userToBlock, blockerId, RelationStatus.friendshipStatuses())
    }
  }

  def unblock(blockerId: Int, userToUnblock: Int): Future[Int] =
    relationsRepository.deleteWithStatus(blockerId, userToUnblock, Seq(RelationStatus.BLOCKED))

  def getBlockedUsers(usrid: Int): Future[Seq[User]] = relationsRepository.getBlockedUsers(usrid)

}
