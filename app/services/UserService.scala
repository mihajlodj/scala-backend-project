package services

import model.User
import model.dto.NewPasswordDTO
import model.exceptions.user.{LoginFailureException, PasswordChangeFailed, RegistrationFailureException}
import repositories.UserRepository

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import org.mindrot.jbcrypt.BCrypt
import security.JwtUtils

import java.sql.SQLIntegrityConstraintViolationException
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class UserService @Inject() (userRepository: UserRepository, blockService: BlockService, passwordService: PasswordService) {
  def searchUsers(username: String, usrid: Int): Future[Seq[User]] = {
    userRepository.searchUsers(username, usrid).flatMap {
      users =>
        blockService.getBlockedUsers(usrid).map {
          blockedUsers =>
            val blockedIds = blockedUsers.map(_.id)
            users.filterNot(u => blockedIds.contains(u.id))
        }
    }
  }

  def attemptLogin(username: String, password: String): Future[String] = {
    getByUsername(username).map {
      case Some(user) =>
        if (BCrypt.checkpw(password, user.password))
          JwtUtils.createToken(user)
        else
          throw new LoginFailureException("Invalid password")
      case None =>
        throw new LoginFailureException("Username doesn't exist")
    }
  }

  def register(username: String, password1: String, password2: String): Future[Int] = {
    if (password1.equals(password2)) {
      val passwordHash = BCrypt.hashpw(password1, BCrypt.gensalt)
      val newUser = User(-1, username, passwordHash)
      create(newUser).recover({
        case _: SQLIntegrityConstraintViolationException => throw new RegistrationFailureException("Username taken")
      })
    } else {
      Future.failed(new RegistrationFailureException("Passwords don't match"))
    }
  }

  def attemptPasswordChange(dto: NewPasswordDTO, usrid: Int): Future[Option[User]] = {
    val oldPassword = dto.oldPassword
    val newPassword1 = dto.newPassword1
    val newPassword2 = dto.newPassword2

    if (!newPassword1.equals(newPassword2))
      Future.failed(new PasswordChangeFailed("New passwords don't match"))
    else if (oldPassword.equals(newPassword1))
      Future.failed(new PasswordChangeFailed("Old and new password are the same"))

    else getById(usrid).flatMap {
      case Some(u) =>
        if (!passwordService.checkpw(oldPassword, u.password)) throw new PasswordChangeFailed("Old passwords don't match")
        else {
          val newPassword = passwordService.hashpw(newPassword1)
          val updatedUser = u.copy(password=newPassword)
          update(usrid, updatedUser)
        }
      case None => Future.failed(throw new PasswordChangeFailed("User doesn't exist"))
    }

  }


  def getAll: Future[Seq[User]] = userRepository.getAll

  def getById(id: Int): Future[Option[User]] = userRepository.getById(id)
  def getByUsername(username: String): Future[Option[User]] = userRepository.getByUsername(username)

  def create(user: User): Future[Int] = userRepository.insert(user)

  def delete(id: Int): Future[Option[Int]] = userRepository.delete(id)

  def update(id: Int, user: User): Future[Option[User]] = userRepository.update(id, user)

}
