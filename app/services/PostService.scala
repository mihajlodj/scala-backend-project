package services

import model.{Post, RelationStatus}
import model.dto.PostDTO
import model.exceptions.friends.NotFriendsException
import model.profiles.PostProfile
import repositories.{RelationsRepository, LikesRepository, PostRepository}

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import java.time.LocalDateTime
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class PostService @Inject() (postRepository: PostRepository, friendRepository: RelationsRepository,
                             friendService: FriendService, likesRepository: LikesRepository){

  def getFriendsTimeline(userid: Int, friendid: Int, page: Int, pageSize: Int): Future[Seq[PostProfile]] = {
    friendRepository.getByUsersAndStatus(userid, friendid, Seq(RelationStatus.ACCEPTED)).flatMap {
      result => if (result.isEmpty) {throw new NotFriendsException("You are not friends with this person")}
        postRepository.getTimeline(friendid, page, pageSize).flatMap {
          f => convertToTimelineDTO(userid, f)
        }
    }
  }

  private def setUserLikedField(p: PostProfile, b: Boolean): PostProfile = {
    p.copy(userLiked = b)
  }

  def updatePost(dto: PostDTO, posterId: Int): Future[Option[Post]] = {
    val id = dto.id
    getById(id).flatMap {
        case Some(p) =>
          val updatedPost = p.copy(caption=dto.caption)
          update(id, updatedPost, posterId)
        case None => Future.successful(None)
      }
    }

  def createPost(dto: PostDTO, posterId: Int): Future[Int] = {
    val post = Post(posterId=posterId, caption=dto.caption, edited=false, deleted=false, datePosted=LocalDateTime.now())
    create(post)
  }


  def getFullTimeline(usrid: Int, page: Int, pageSize: Int): Future[Seq[PostProfile]] = {
    friendService.getFriends(usrid).flatMap {
      friends => getFriendsPosts(usrid, page, pageSize, friends)

    }
  }
  private def getFriendsPosts(usrid: Int, page: Int, pageSize: Int, friends: Seq[Int]): Future[Seq[PostProfile]] = {
    val posterIds = friends :+ usrid
    postRepository.getFullTimeline(posterIds, page, pageSize).flatMap(
      posts => convertToTimelineDTO(usrid, posts)
    )
  }
  private def convertToTimelineDTO(usrid: Int, posts: Seq[PostProfile]): Future[Seq[PostProfile]] = {
    Future.sequence(posts.map { p => likesRepository.hasUserLikedPost(usrid, p.id).map(res => setUserLikedField(p, res)) })
  }

  def getById(id: Int): Future[Option[Post]] = postRepository.getById(id)

  def create(post: Post): Future[Int] = postRepository.insert(post)

  def delete(id: Int, posterId: Int): Future[Option[Int]] = postRepository.delete(id, posterId)

  def update(id: Int, post: Post, posterId: Int): Future[Option[Post]] = postRepository.update(id, post, posterId)

}
