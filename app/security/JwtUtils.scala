package security

import model.User
import pdi.jwt._
import play.api.libs.json.Json

import java.nio.charset.StandardCharsets
import java.time.Clock
import javax.crypto.spec.SecretKeySpec
import scala.util.{Failure, Success}

object JwtUtils {
  implicit val clock: Clock = Clock.systemUTC
  private val secretKey = "6QC3vJ_r(&R~Fk/8?&HyroH>i+imkAf=`XiPd8a=k@|t-KY_fB`k%_)x/XKSu+*8"
  private val secretKeyBytes = secretKey.getBytes(StandardCharsets.UTF_8)
  private val secretKeyVal = new SecretKeySpec(secretKeyBytes, "AES")

  def createToken(u: User): String = {
    val payload = Json.stringify(Json.obj("username" -> u.username, "id" -> u.id))
    createToken(payload)
  }

  private def createToken(payload: String): String = {
    val jwt = JwtJson.encode(JwtHeader(JwtAlgorithm.HS256), JwtClaim(payload).issuedNow.expiresIn(7200), secretKey)
    jwt
  }

  def isTokenValid(bearer_token: String): Boolean = {
    val token = sliceBearer(bearer_token)
    Jwt.isValid(token, secretKeyVal)
  }

  def parsePayload(bearer_token: String): Option[Int] = {
    val token = sliceBearer(bearer_token)
    val payload = JwtJson.decodeJson(token, secretKeyVal)
    payload match {
      case Success(v) => (v \ "id").asOpt[Int]
      case Failure(_) => None
    }
  }

  private def sliceBearer(bearer_token: String): String = bearer_token.slice(7, bearer_token.length)

}
