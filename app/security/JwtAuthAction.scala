package security

import play.api.libs.json.Json
import play.api.mvc._

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

case class AuthorizedRequest[A](usrid: Int, request: Request[A]) extends WrappedRequest[A](request)

class JwtAuthAction @Inject()(parser: BodyParsers.Default)(implicit ec: ExecutionContext)
  extends ActionBuilder[AuthorizedRequest, AnyContent] {
  override def invokeBlock[A](request: Request[A], block: AuthorizedRequest[A] => Future[Result]): Future[Result] = {
      request.headers.get("Authorization") match {
        case Some(bearer_token) if JwtUtils.isTokenValid(bearer_token) =>
          JwtUtils.parsePayload(bearer_token) match {
            case Some(payload) => block(AuthorizedRequest(payload, request))
            case None => Future.successful(Results.Forbidden(Json.toJson("message" -> "Invalid token payload")))
          }
        case Some(_) => Future.successful(Results.Forbidden(Json.toJson("message" -> "Invalid token")))
        case None => Future.successful(Results.Unauthorized(Json.toJson("message" -> "Missing token")))
      }
    }
  override def parser: BodyParser[AnyContent] = parser
  override protected def executionContext: ExecutionContext = ec
}