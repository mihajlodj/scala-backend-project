package controllers

import model.dto.CommentDTO
import play.api.mvc.{AbstractController, Action, ControllerComponents, _}
import security.{AuthorizedRequest, JwtAuthAction}
import play.api.libs.json._
import services.CommentService

import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CommentController @Inject() (cc: ControllerComponents, jwtAuthAction: JwtAuthAction,
                                   commentService: CommentService) extends AbstractController(cc) {

  def postComment(): Action[CommentDTO] = jwtAuthAction.async(parse.json[CommentDTO]) { request: AuthorizedRequest[CommentDTO] =>
    val newComment = request.body.comment
    val postId = request.body.postId
    commentService.postComment(request.usrid,postId, newComment).map(_ => Ok(Json.toJson("message", "Comment posted")))
  }

  def editComment(): Action[CommentDTO] = jwtAuthAction.async(parse.json[CommentDTO]) { request: AuthorizedRequest[CommentDTO] =>
    val newComment = request.body.comment
    val opt_id = request.body.id
    opt_id match {
      case Some(id) =>
        commentService.updateComment(id, request.usrid, newComment).map {
          case Some(_) => Ok(Json.toJson("message", "Comment edited"))
          case None => NotFound(Json.toJson("message" -> "Comment not found"))
        }
      case None => Future.successful(BadRequest(Json.toJson("message" -> "Comment id not passed")))
    }
  }

  def deleteComment(id: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val commenterId = request.usrid
    commentService.deleteComment(id, commenterId).map {
      case Some(_) => Ok(Json.toJson("message" -> "Comment deleted"))
      case None => NotFound(Json.toJson("message" -> "Comment not found"))
    }
  }

  def getComments(postId: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    commentService.getComments(postId).map {
      comments => Ok(Json.toJson(comments))
    }
  }


}
