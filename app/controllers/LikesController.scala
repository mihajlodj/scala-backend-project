package controllers
import model.dto.LikeDTO
import model.exceptions.friends.NotFriendsException
import model.exceptions.likes._
import model.exceptions.post.PostNotFound
import play.api.libs.json._
import play.api.mvc._
import security.{AuthorizedRequest, JwtAuthAction}
import services._

import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
class LikesController @Inject() (cc: ControllerComponents, jwtAuthAction: JwtAuthAction,
                                 likesService: LikesService) extends AbstractController(cc) {

  def likePost(): Action[LikeDTO] = jwtAuthAction.async(parse.json[LikeDTO]) { request: AuthorizedRequest[LikeDTO] =>
    val usrid = request.usrid
    val posterId = request.body.posterid
    val postid = request.body.postid
    likesService.like(usrid, postid, posterId).map({
      _ => Ok(Json.toJson("message" -> "Post liked"))
    }).recover {
      case e1: PostLikedException => BadRequest(Json.toJson("message" -> e1.getMessage))
      case e2: NotFriendsException => BadRequest(Json.toJson("message" -> e2.getMessage))
      case e3: PostNotFound => BadRequest(Json.toJson("message" -> e3.getMessage))
    }
  }

  def unlikePost(): Action[LikeDTO] = jwtAuthAction.async(parse.json[LikeDTO]) { request: AuthorizedRequest[LikeDTO] =>
    val usrid = request.usrid
    val posterId = request.body.posterid
    val postid = request.body.postid
    likesService.unlike(usrid, postid, posterId).map({
      _ => Ok(Json.toJson("message" -> "Post unliked"))
    }).recover {
      case e1: LikeDoesntExistException => BadRequest(Json.toJson("message" -> e1.getMessage))
      case e2: NotFriendsException => BadRequest(Json.toJson("message" -> e2.getMessage))
      case e3: PostNotFound => BadRequest(Json.toJson("message" -> e3.getMessage))
    }
  }

}
