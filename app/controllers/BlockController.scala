package controllers

import model.exceptions.block.BlockFailedException
import model.profiles.UserProfile
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import security.{AuthorizedRequest, JwtAuthAction}

import scala.concurrent.ExecutionContext.Implicits.global
import services.BlockService

import javax.inject.Inject

class BlockController @Inject() (cc: ControllerComponents, jwtAuthAction: JwtAuthAction,
                                 blockService: BlockService) extends AbstractController(cc) {

  def block(userToBlock: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val blockerId = request.usrid
    blockService.block(blockerId, userToBlock).map(_ => Ok(Json.toJson("message" -> "User blocked"))).recover {
      case e: BlockFailedException => BadRequest(Json.toJson("message" -> e.getMessage))
    }
  }

  def unblock(userToUnblock: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val blockerId = request.usrid
    blockService.unblock(blockerId, userToUnblock).map {
      case i if i > 0 => Ok(Json.toJson("message" -> "User unblocked"))
      case 0 => BadRequest(Json.toJson("message" -> "User is not blocked"))
    }
  }

  def getBlockedUsers(): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    blockService.getBlockedUsers(request.usrid).map {
      users => Ok(Json.toJson(users.map(u => UserProfile.fromUser(u))))
    }
  }

}
