package controllers

import model.Post
import model.dto.PostDTO
import model.exceptions.friends.NotFriendsException
import model.profiles.PostProfile
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import security.{AuthorizedRequest, JwtAuthAction}
import services.{PostService, UserService}

import scala.concurrent.ExecutionContext.Implicits.global
import javax.inject.{Inject, Singleton}

@Singleton
class PostController @Inject() (cc: ControllerComponents, userService: UserService,
                                jwtAuthAction: JwtAuthAction, postService: PostService)
  extends AbstractController(cc) {


  def makePost(): Action[PostDTO] = jwtAuthAction.async(parse.json[PostDTO])  { request: AuthorizedRequest[PostDTO] =>
    val dto: PostDTO = request.body
    postService.createPost(dto, request.usrid).map {
      id: Int =>
        Ok(Json.toJson("postId" -> id))
    }
  }

  def timeline(page: Int, pageSize: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val posterId = request.usrid
    postService.getFullTimeline(posterId, page, pageSize).map {
      results => Ok(Json.toJson(results))
    }
  }

  def friendsTimeline(page: Int, pageSize: Int, friendId: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val userid = request.usrid
    postService.getFriendsTimeline(userid, friendId, page, pageSize).map({
      results =>
        Ok(Json.toJson(results))
    }).recover {
      case e: NotFriendsException => Ok(Json.toJson("message" -> e.getMessage))
    }
  }

  def deletePost(postId: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    postService.delete(postId, request.usrid).map {
      case Some(_) => Ok(Json.toJson("message" -> "Post deleted"))
      case None => NotFound(Json.toJson("message" -> "Post for deletion not found"))
    }
  }

  def updatePost(): Action[PostDTO] = jwtAuthAction.async(parse.json[PostDTO])  { request: AuthorizedRequest[PostDTO] =>
    val dto: PostDTO = request.body
    val posterId = request.usrid
    postService.updatePost(dto, posterId).map {
      case Some(p) => Ok(Json.toJson(p))
      case None => NotFound(Json.toJson("message" -> "Post for update not found"))
    }
  }
}
