package controllers

import model.dto.LoginDTO
import model.exceptions.user.LoginFailureException
import play.api.libs.json._
import play.api.mvc._
import services.UserService

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class LoginController @Inject() (cc: ControllerComponents, userService: UserService) extends AbstractController(cc) {

  def login(): Action[LoginDTO] = Action.async(parse.json[LoginDTO]) { request: Request[LoginDTO] =>
    val dto: LoginDTO = request.body
    val username = dto.username
    val password = dto.password
    userService.attemptLogin(username, password).map ({
      token: String =>
        Ok(Json.toJson("jwt" -> token))
    }).recover {
      case e: LoginFailureException =>
        println(e.getMessage)
        Unauthorized(Json.toJson("message"-> "Invalid username or password"))
    }
  }

}
