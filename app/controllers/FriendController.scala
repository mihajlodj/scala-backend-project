package controllers

import model.dto.FriendRequestDTO
import model.exceptions.friends.{FriendRequestAnswered, FriendRequestExistsException, NoFriendRequestException}
import model.profiles.UserProfile
import play.api.libs.json._
import play.api.mvc._
import security.{AuthorizedRequest, JwtAuthAction}
import services.FriendService

import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
class FriendController @Inject() (cc: ControllerComponents, jwtAuthAction: JwtAuthAction,
                                  friendService: FriendService) extends AbstractController(cc) {

  def sendFriendRequest(): Action[FriendRequestDTO] = jwtAuthAction.async(parse.json[FriendRequestDTO]) { request: AuthorizedRequest[FriendRequestDTO] =>
    val senderId = request.usrid
    val receiverId = request.body.receiverId
    friendService.sendRequest(senderId, receiverId).map({
      _ => Ok(Json.toJson("message" -> "Friend request sent!"))
    }).recover {
      case e: FriendRequestExistsException => BadRequest(Json.toJson("message" -> e.getMessage))
    }
  }

  def acceptRequest(requestId: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val receiverId = request.usrid

    friendService.acceptRequest(requestId, receiverId).map({
      case Some(_) => Ok(Json.toJson("message" -> "Friend request accepted!"))
      case None => NotFound
    }).recover {
      case e: FriendRequestAnswered => BadRequest(Json.toJson("message" -> e.getMessage))
      case f: NoFriendRequestException => BadRequest(Json.toJson("message" -> f.getMessage))
    }
  }

  def rejectRequest(requestId: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val receiverId = request.usrid

    friendService.rejectRequest(requestId, receiverId).map({
      case Some(_) => Ok(Json.toJson("message" -> "Friend request rejected!"))
      case None => NotFound
    }).recover {
      case e: FriendRequestAnswered => BadRequest(Json.toJson("message" -> e.getMessage))
      case f: NoFriendRequestException => BadRequest(Json.toJson("message" -> f.getMessage))
    }
  }

  def getFriendRequests(): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val receiverId = request.usrid
    friendService.getPendingFriendRequests(receiverId).map {
      requests => Ok(Json.toJson(requests.map(r => FriendRequestDTO.fromRequest(r))))
    }
  }

  def getFriends: Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val receiverId = request.usrid
    friendService.getUserFriends(receiverId).map {
      requests => Ok(Json.toJson(requests.map(u => UserProfile.fromUser(u))))
    }
  }

  def friendsProfile(friendId: Int): Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val userid = request.usrid
    friendService.getFriendProfile(userid, friendId).map({
      case Some(u) => Ok(Json.toJson(UserProfile.fromUser(u)))
      case None => BadRequest(Json.toJson("message" -> "User doesn't exist"))
    }).recover {
      case e: FriendRequestExistsException => BadRequest(Json.toJson("message" -> e.getMessage))
    }
  }
}
