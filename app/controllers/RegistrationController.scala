package controllers

import model.dto.RegistrationDTO
import model.exceptions.user.RegistrationFailureException
import play.api.libs.json.Json
import play.api.mvc._
import services.UserService

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits.global
@Singleton
class RegistrationController @Inject() (cc: ControllerComponents, userService: UserService) extends AbstractController(cc) {

  def register(): Action[RegistrationDTO] = Action.async(parse.json[RegistrationDTO]) { request: Request[RegistrationDTO] =>
    val dto: RegistrationDTO = request.body
    userService.register(dto.username, dto.password1, dto.password2).map (
      e => {
      println(e)
        Ok
    }).recover {
      case e: RegistrationFailureException => BadRequest(Json.toJson("message" -> e.getMessage))
    }
  }
}
