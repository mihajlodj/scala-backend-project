package controllers

import model.dto.NewPasswordDTO
import model.exceptions.user.PasswordChangeFailed
import model.profiles.UserProfile
import play.api.libs.json._
import play.api.mvc._
import security.{AuthorizedRequest, JwtAuthAction}
import services.UserService

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class UserController @Inject() (cc: ControllerComponents, userService: UserService, jwtAuthAction: JwtAuthAction)
  extends AbstractController(cc) {

  def getUserProfile: Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    userService.getById(request.usrid).map {
      case Some(u) => Ok(Json.toJson(UserProfile.fromUser(u)))
      case None => NotFound(Json.toJson("message" -> s"User with id: ${request.usrid} doesn't exist"))
    }
  }

  def searchUsers() : Action[AnyContent] = jwtAuthAction.async { request: AuthorizedRequest[AnyContent] =>
    val username = request.getQueryString("username").get
    userService.searchUsers(username, request.usrid).map {
      result =>
        Ok(Json.toJson(result.map(u => UserProfile.fromUser(u))))
    }
  }

  def getAllUsers: Action[AnyContent] = jwtAuthAction.async {
    userService.getAll.map(users => Ok(Json.toJson(users.map(u => UserProfile.fromUser(u)))))
  }

  def changePassword(): Action[NewPasswordDTO] = jwtAuthAction.async(parse.json[NewPasswordDTO]){ request: AuthorizedRequest[NewPasswordDTO] =>
    val usrid = request.usrid
    userService.attemptPasswordChange(request.body, usrid).map({
      case Some(_) => Ok(Json.toJson("message" -> "Password changed!"))
      case None => BadRequest(Json.toJson("message" -> "User doesn't exist"))
    }).recover {
      case e: PasswordChangeFailed => BadRequest(Json.toJson("message" -> e.getMessage))
    }
  }
}
