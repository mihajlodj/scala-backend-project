package repositories

import model.RelationStatus.RelationStatus
import model.{RelationStatus, Relationship, User}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}
import java.time.LocalDateTime
import javax.inject.Inject

class RelationsRepository @Inject()(override protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  val friends = TableQuery[FriendTable]

  implicit val statusMapper = MappedColumnType.base[RelationStatus, String](
    e => e.toString,
    s => RelationStatus(s)
  )

  def getById(id: Int): Future[Option[Relationship]] = db.run(friends.filter(_.id === id).result).map(_.headOption)
  def getByUsersAndStatus(id1: Int, id2: Int, status: Seq[RelationStatus]): Future[Seq[Relationship]] = {
      db.run(friends.filter(p =>
        (p.senderId === id1 || p.senderId === id2) &&
        (p.receiverId === id1 || p.receiverId === id2) &&
        p.status.inSet(status)
      ).result)
  }
  def getByReceiverIdAndStatus(id: Int, status: RelationStatus): Future[Seq[Relationship]]
  = db.run(friends.filter(p =>
    p.receiverId === id &&
    p.status === status
  ).result)

  def getBySenderAndReceiverId(senderId: Int, receiverId: Int): Future[Option[Relationship]]
  = db.run(friends.filter(p =>
    p.senderId === senderId && p.receiverId === receiverId
  ).result).map(_.headOption)

  def getAcceptedFriendRequests(usrid: Int): Future[Seq[User]] = {
    val query = sql"""
          SELECT u.* FROM socialnetworkdb.relationships AS fr
          INNER JOIN socialnetworkdb.users AS u ON fr.receiverId = u.id
            WHERE (#$usrid=fr.senderId OR #$usrid=fr.receiverId) AND fr.status='ACCEPTED';
        """.as[User]
    db.run(query)
  }

  def getBlockedUsers(usrid: Int): Future[Seq[User]] = {
    val query = sql"""
          SELECT u.* FROM socialnetworkdb.relationships AS fr
          INNER JOIN socialnetworkdb.users AS u ON fr.receiverId = u.id
            WHERE $usrid=fr.senderId AND fr.status='BLOCKED';
        """.as[User]
    db.run(query)
  }

  def getUserFriends(usrid: Int): Future[Seq[Int]] = {
    val query = sql"""
          SELECT receiverId FROM socialnetworkdb.relationships AS fr
            WHERE $usrid=fr.senderId AND fr.status='ACCEPTED'
          UNION
          SELECT senderId FROM socialnetworkdb.relationships AS fr
              WHERE $usrid=fr.receiverId AND fr.status='ACCEPTED';
        """.as[Int]
    db.run(query)
  }


  def insert(request: Relationship): Future[Int] =
    db.run(friends returning friends.map(_.id) += request)

  def respondToRequest(id: Int, rel: Relationship, receiverId: Int): Future[Option[Relationship]] = {
    // samo primalac friend requesta moze da ga updateuje (tj. da mu promeni status)
    db.run(friends.filter(p => p.id === id && p.receiverId === receiverId).update(rel).map {
      case 0 => None
      case 1 => Some(rel)
      case updated => throw new RuntimeException(s"Updated $updated rows")
    })
  }

  def update(relation: Relationship): Future[Int] = {
    db.run(friends.filter(_.id === relation.id).update(relation).map {
      case 0 => 0
      case 1 => 1
      case updated => throw new RuntimeException(s"Updated $updated rows")
    })
  }

  def deleteWithStatus(senderId: Int, receiverId: Int, statuses: Seq[RelationStatus]): Future[Int] = {
    db.run(friends.filter(p => p.senderId === senderId && p.receiverId === receiverId && p.status.inSet(statuses)).delete)
  }

  class FriendTable(tag: Tag) extends Table[Relationship](tag, None, "relationships") {
    implicit val statusMapper = MappedColumnType.base[RelationStatus, String](
      e => e.toString,
      s => RelationStatus(s)
    )
    val id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    val senderId = column[Int]("senderId")
    val receiverId = column[Int]("receiverId")
    val status = column[RelationStatus]("status")
    val date = column[LocalDateTime]("date")

    override def * =
      (id, senderId, receiverId, status, date) <> (Relationship.tupled, Relationship.unapply)


  }

}
