package repositories
import model.Post
import model.exceptions.post.PostNotFound
import model.profiles.PostProfile
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
import java.time.LocalDateTime
class PostRepository @Inject() (override protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] {


  import profile.api._

  val posts = TableQuery[PostTable]

  def getFullTimeline(posterIds: Seq[Int], page: Int, pageSize: Int): Future[Seq[PostProfile]] = {
    val offset = (page-1) * pageSize
    val query =
      sql"""
        SELECT socialnetworkdb.posts.*, 'false' AS userLiked, u.username
        FROM socialnetworkdb.posts AS posts
        INNER JOIN socialnetworkdb.users AS u ON u.id = posts.posterId
        WHERE posts.posterId IN #${posterIds.mkString("(", ", ", ")")}
          AND posts.deleted = 0
        ORDER BY posts.datePosted DESC
        LIMIT $pageSize OFFSET $offset;
         """.as[PostProfile]
    db.run(query)
  }

  def incrementLikes(postid: Int): Future[Any] = {
    val fetchQuery = posts.filter(_.id === postid).map(_.likes).result.headOption
    db.run(fetchQuery).flatMap {
      case Some(curLikes) =>
        val updatedLikes = curLikes + 1
        val updateQuery = posts.filter(_.id === postid).map(_.likes).update(updatedLikes)
        db.run(updateQuery)
      case None =>
        throw new PostNotFound("There is no such post to like") // No matching row found, so no update needed
    }
  }

  def decrementLikes(postid: Int): Future[Any] = {
    val fetchQuery = posts.filter(_.id === postid).map(_.likes).result.headOption
    db.run(fetchQuery).flatMap {
      case Some(curLikes) =>
        val updatedLikes = curLikes - 1
        val updateQuery = posts.filter(_.id === postid).map(_.likes).update(updatedLikes)
        db.run(updateQuery)
      case None =>
        throw new PostNotFound("There is no such post to unlike") // No matching row found, so no update needed
    }
  }

  def getTimeline(posterId: Int, page: Int, pageSize: Int): Future[Seq[PostProfile]] = {
    val offset = (page-1)*pageSize
    val query = sql"""
          SELECT socialnetworkdb.posts.*, 'false' AS userLiked, u.username
          FROM socialnetworkdb.posts AS posts
          INNER JOIN socialnetworkdb.users AS u ON u.id = posts.posterId
          WHERE posts.posterId = $posterId
            AND posts.deleted = 0
          ORDER BY posts.datePosted DESC
          LIMIT $pageSize OFFSET $offset;
        """.as[PostProfile]
    db.run(query)
  }

  def getById(id: Int): Future[Option[Post]] = db.run(posts.filter(_.id === id).result).map(_.headOption)
  def insert(post: Post): Future[Int] =
    db.run(posts returning posts.map(_.id) += post)

  def delete(id: Int, posterId: Int): Future[Option[Int]] = {
    db.run(posts.filter({p => p.id ===id && p.posterId === posterId}).delete).map {
      case 0 => None
      case 1 => Some(1)
      case deleted => throw new RuntimeException(s"Deleted $deleted rows")
    }
  }

  def update(id: Int, post: Post, posterId: Int): Future[Option[Post]] = {
    db.run(posts.filter(p => p.id === id && p.posterId === posterId && p.deleted === false).update(post).map {
      case 0 => None
      case 1 => Some(post)
      case updated => throw new RuntimeException(s"Updated $updated rows")
    })
  }
  class PostTable(tag: Tag) extends Table[Post](tag, None, "posts") {

    val id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    val posterId = column[Int]("posterId")
    val caption = column[String]("caption")
    val edited = column[Boolean]("edited")
    val deleted = column[Boolean]("deleted")
    val datePosted = column[LocalDateTime]("datePosted")
    val likes = column[Int]("likes")

    override def * =
      (id, posterId, caption, edited, deleted, datePosted, likes) <> (Post.tupled, Post.unapply)
  }

}
