package repositories
import model.User
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
class UserRepository @Inject() (override protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] {


  import profile.api._

  val users = TableQuery[UserTable]

  def searchUsers(username: String, usrid: Int): Future[Seq[User]] = {
    val query =
      sql"""
        SELECT u.*
        FROM socialnetworkdb.users AS u
        WHERE u.username LIKE '%#$username%' AND #$usrid NOT IN (
          SELECT rel.receiverId
          FROM relationships AS rel
          WHERE rel.senderId = u.id AND rel.status = 'BLOCKED'
        );
         """.as[User]
    db.run(query)
  }

  def getAll: Future[Seq[User]] = db.run(users.result)

  def getById(id: Int): Future[Option[User]] = db.run(users.filter(_.id === id).result).map(_.headOption)
  def getByUsername(username: String): Future[Option[User]] = db.run(users.filter(_.username === username).result).map(_.headOption)
  def insert(user: User): Future[Int] =
    db.run(users returning users.map(_.id) += user)

  def delete(id: Int): Future[Option[Int]] = {
    db.run(users.filter(_.id === id).delete).map {
      case 0 => None
      case 1 => Some(1)
    }
  }

  def update(id: Int, user: User): Future[Option[User]] = {
    db.run(users.filter(_.id === id).update(user).map {
      case 0 => None
      case 1 => Some(user)
      case updated => throw new RuntimeException(s"Updated $updated rows")
    })
  }
  class UserTable(tag: Tag) extends Table[User](tag, None, "users") {

    val id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    val username = column[String]("username", O.Unique)
    val password = column[String]("password")

    override def * =
      (id, username, password) <> (User.tupled, User.unapply)
  }

}
