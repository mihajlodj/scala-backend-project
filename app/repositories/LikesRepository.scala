package repositories
import model.UserPostLikes
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class LikesRepository @Inject()(override protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  val likes = TableQuery[LikesTable]

  def hasUserLikedPost(usrid: Int, postid: Int): Future[Boolean] =
    db.run(likes.filter(p => p.usrid === usrid && p.postid === postid).length.===(1).result)

  def insert(like: UserPostLikes): Future[Int] =
    db.run(likes += like)

  def delete(usrid: Int, postid: Int): Future[Boolean] = {
    db.run(likes.filter({ p => p.usrid === usrid && p.postid === postid }).delete).map {
      case 0 => false
      case 1 => true
    }
  }

  class LikesTable(tag: Tag) extends Table[UserPostLikes](tag, None, "user_post_likes") {

    val usrid = column[Int]("usrid")
    val postid = column[Int]("postid")

    def pk = primaryKey("pk_a", (usrid, postid))

    override def * =
      (usrid, postid) <> (UserPostLikes.tupled, UserPostLikes.unapply)
  }
}
