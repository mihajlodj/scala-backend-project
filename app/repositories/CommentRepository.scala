package repositories

import model.Comment
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import java.time.LocalDateTime
import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class CommentRepository @Inject() (override protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  val comments = TableQuery[CommentTable]

  def getById(id: Int): Future[Option[Comment]] = db.run(comments.filter(_.id === id).result).map(_.headOption)

  def getPostComments(postId: Int): Future[Seq[Comment]] = {
    db.run(comments.filter(_.postId === postId).result)
  }

  def insert(post: Comment): Future[Int] =
    db.run(comments returning comments.map(_.id) += post)

  def delete(id: Int, commenterId: Int): Future[Option[Int]] = {
    db.run(comments.filter({ p => p.id === id && p.commenterId === commenterId }).delete).map {
      case 0 => None
      case 1 => Some(1)
    }
  }

  def update(id: Int, com: Comment, commenterId: Int): Future[Option[Comment]] = {
    db.run(comments.filter(p => p.id === id && p.commenterId === commenterId).update(com).map {
      case 0 => None
      case 1 => Some(com)
    })
  }

  class CommentTable(tag: Tag) extends Table[Comment](tag, None, "comments") {

    val id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    val postId = column[Int]("postId")
    val commenterId = column[Int]("commenterId")
    val comment = column[String]("comment")
    val datePosted = column[LocalDateTime]("datePosted")

    override def * =
      (id, postId, commenterId, comment, datePosted) <> (Comment.tupled, Comment.unapply)
  }

}
